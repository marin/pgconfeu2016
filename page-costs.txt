Page costs

postgresql.conf:
listen_addresses = '*'

pg_ident.conf - empty

pg_hba.conf:
local   all             all                                     trust
host    all             all             0.0.0.0/0               trust
host    all             all             ::0/0                   trust



-- create example tables
DROP TABLE IF EXISTS a CASCADE;

CREATE TABLE a(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    type_id SMALLINT
);

DROP TABLE IF EXISTS b CASCADE;

CREATE TABLE b(
    id SERIAL NOT NULL PRIMARY KEY,
    type_id SMALLINT,
    a_id INT REFERENCES a(id),
    some_date CHAR(10)
);

CREATE INDEX b_a ON b(a_id);

-- populate tables with data
-- use setseed to have repeatable results
SELECT setseed(0.314159);

-- add 1M rows to a
INSERT INTO a(type_id)
SELECT (random()*10+1)::int FROM generate_series(1, 1000000) i;

-- add 2M rows to b
INSERT INTO b(type_id, a_id, some_date)
SELECT (random()*10+1)::int, i%1000000+1,  i::CHAR(10) FROM generate_series(1, 2000000) i;

-- get fresh statistics
ANALYSE a;
ANALYSE b;

-- basic example
EXPLAIN ANALYSE
SELECT a.*
FROM a
WHERE a.id <= 550000;

/*
"Seq Scan on a  (cost=0.00..17906.00 rows=552316 width=10) (actual time=0.148..116.058 rows=550000 loops=1)"
"  Filter: (id <= 550000)"
"  Rows Removed by Filter: 450000"
"Planning time: 0.110 ms"
"Execution time: 138.030 ms"
*/

SET seq_page_cost = 1000;
SET random_page_cost = 1;

EXPLAIN ANALYSE
SELECT a.*
FROM a
WHERE a.id <= 550000;

/*
"Index Scan using a_pkey on a  (cost=0.42..2996183.96 rows=552316 width=10) (actual time=0.207..99.242 rows=550000 loops=1)"
"  Index Cond: (id <= 550000)"
"Planning time: 0.107 ms"
"Execution time: 120.753 ms"
*/

EXPLAIN ANALYSE
SELECT a.*
FROM a
WHERE a.id <= 800000;

/*
"Index Scan using a_pkey on a  (cost=0.42..4351238.95 rows=802030 width=10) (actual time=0.575..139.901 rows=800000 loops=1)"
"  Index Cond: (id <= 800000)"
"Planning time: 0.155 ms"
"Execution time: 169.954 ms"
*/